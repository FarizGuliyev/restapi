package IngressTask.RestApi.repositories;

import IngressTask.RestApi.entities.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeacherRepository extends JpaRepository<Teacher, Integer> {
}
