package IngressTask.RestApi.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;

@Data
public class TeacherDto {
    private int id;
    private String name;
    private String faculty;

}
