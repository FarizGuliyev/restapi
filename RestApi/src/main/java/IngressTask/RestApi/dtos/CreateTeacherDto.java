package IngressTask.RestApi.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;

@Data
public class CreateTeacherDto {
    private int id;
    private String name;
    private String faculty;
    @JsonFormat(pattern = "dd.MM.yyyy")
    private LocalDate birthdate;
}
