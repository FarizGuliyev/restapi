package IngressTask.RestApi.entities;

public final class SingletonPattern {
    private static SingletonPattern singleton;

    private SingletonPattern() {

    }

    public static SingletonPattern singletonPattern() {

        if (singleton == null) {
            synchronized (SingletonPattern.class) {
                if (singleton == null) {
                    singleton = new SingletonPattern();
                }
            }
        }
        return singleton;
    }
}
