package IngressTask.RestApi;

import IngressTask.RestApi.configuration.AppConfig;
import IngressTask.RestApi.services.InterfaceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestApiApplication implements CommandLineRunner {
    private final InterfaceImpl anInterface;

//    @Value("${ms.name}")
//    private String name;
//
//    @Value("${ms.student}")
//    private String st;

    private final AppConfig appConfig;

    public RestApiApplication(InterfaceImpl classImpl,AppConfig appConfig) {
        this.anInterface = classImpl;
        this.appConfig=appConfig;
    }

    public static void main(String[] args) {
        SpringApplication.run(RestApiApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Teacher service is:" + anInterface);
        System.out.println("Ms name is " + appConfig.getName());
        System.out.println("Ms student is " + appConfig.getStudent());
        System.out.println("Ms students is " + appConfig.getStudents());
    }
}
