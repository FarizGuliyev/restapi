package IngressTask.RestApi.controllers;

import IngressTask.RestApi.dtos.CreateTeacherDto;
import IngressTask.RestApi.dtos.TeacherDto;
import IngressTask.RestApi.services.ITeacherService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/teacher")
public class TeacherController {

    private final ITeacherService service;

    public TeacherController(ITeacherService service) {
        this.service = service;
    }

    @PostMapping
    public void create(@RequestBody CreateTeacherDto createTeacherDto) {
        service.createTeacher(createTeacherDto);
    }

    @GetMapping("/{id}")
    public TeacherDto getTeacherById(@PathVariable Integer id) {
        return service.getTeacherById(id);
    }

    @GetMapping
    public List<TeacherDto> getAllTeachers() {
        return service.getAllTeachers();
    }

    @PutMapping("/{id}")
    public void updateTeacher(@PathVariable Integer id, @RequestBody CreateTeacherDto createTeacherDto) {
        service.updateTeacher(id, createTeacherDto);
    }

    @DeleteMapping("/{id}")
    public String deleteTeacher(@PathVariable Integer id) {
        return service.deleteTeacher(id);
    }
}
