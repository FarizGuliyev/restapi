package IngressTask.RestApi.services;

import IngressTask.RestApi.dtos.CreateTeacherDto;
import IngressTask.RestApi.dtos.TeacherDto;

import java.util.List;


public interface ITeacherService {
    public void createTeacher(CreateTeacherDto createTeacherDto);
    public TeacherDto getTeacherById(Integer id);
    public List<TeacherDto> getAllTeachers();
    public void updateTeacher(Integer id, CreateTeacherDto createTeacherDto);
    public String deleteTeacher(Integer id);

}
