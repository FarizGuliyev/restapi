package IngressTask.RestApi.services;

import IngressTask.RestApi.entities.Teacher;
import IngressTask.RestApi.dtos.CreateTeacherDto;
import IngressTask.RestApi.dtos.TeacherDto;
import IngressTask.RestApi.repositories.TeacherRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TeacherService implements ITeacherService {
    private final TeacherRepository repository;
    private final ModelMapper modelMapper;


    public TeacherService(TeacherRepository repository, ModelMapper modelMapper) {
        this.repository = repository;
        this.modelMapper = modelMapper;
    }

    @Override
    public void createTeacher(CreateTeacherDto createTeacherDto) {
        Teacher teacher = modelMapper.map(createTeacherDto, Teacher.class);
        repository.save(teacher);
    }

    @Override
    public TeacherDto getTeacherById(Integer id) {
        Teacher teacher = repository.findById(id).orElse(null);
        return modelMapper.map(teacher, TeacherDto.class);
    }

    @Override
    public List<TeacherDto> getAllTeachers() {
        List<Teacher> teacherList = repository.findAll();
        List<TeacherDto> teacherDtoList=new ArrayList<>();
        modelMapper.map(teacherList,teacherDtoList);
        return teacherDtoList;
    }

    @Override
    public void updateTeacher(Integer id, CreateTeacherDto createTeacherDto) {
        Optional<Teacher> teacher1 = repository.findById(id);
        teacher1.ifPresent(teacher -> {
            teacher.setName(createTeacherDto.getName());
            teacher.setFaculty(createTeacherDto.getFaculty());
            teacher.setBirthdate(createTeacherDto.getBirthdate());
            repository.save(teacher);
        });
    }

    @Override
    public String deleteTeacher(Integer id) {
        repository.deleteById(id);
        return "Teacher is deleted";
    }

}
